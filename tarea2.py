from math import sqrt

a = float(input("coeficiente A:"))
b = float(input("coeficiente B:"))
c = float(input("coeficiente C:"))
d = b**2-4*a*c
if d<0:
	real = (-b)/(2*a)
	imaginaria = sqrt(-d)/(2*a)
	print("Raiz 1: ",real,"+",imaginaria,"i")
	print("Raiz 2: ",real,"-",imaginaria,"i")
else:
	if d==0:
		r = (-b)/(2*a)
		print("Raiz 1 = Raiz 2: ",r)
	else:
		r1 = ((-b)+sqrt(d))/(2*a)
		r2 = ((-b)-sqrt(d))/(2*a)
		print("Raiz 1: ",r1)
		print("Raiz 2: ",r2)

